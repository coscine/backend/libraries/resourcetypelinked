﻿using Coscine.Configuration;
using Coscine.Metadata;
using Coscine.ResourceTypeBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Query;

namespace Coscine.ResourceTypeLinked
{
    public class ResourceTypeLinked : ResourceTypeDefinition
    {
        private readonly string _urlPrefix = "https://hdl.handle.net/";
        private readonly string _prefix;
        private readonly string _predicate = "https://purl.org/coscine/terms/linked#body";
        private readonly RdfStoreConnector _rdfStoreConnector;

        public ResourceTypeLinked(string name, IConfiguration gConfig, ResourceTypeConfigurationObject resourceTypeConfiguration) : base(name, gConfig, resourceTypeConfiguration)
        {
            _rdfStoreConnector = new RdfStoreConnector(gConfig.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            _prefix = gConfig.GetStringAndWait("coscine/global/epic/prefix");
        }

        private string GetGraphName(string id, string key)
        {
            return $"{GetSubjectName(id, key)}&data";
        }
        private string GetGraphNameForFilter(string id, string key)
        {
            return $"{GetSubjectName(id, key)}.*&data";
        }

        private string GetSubjectName(string id, string key)
        {
            return $"{_urlPrefix}{_prefix}/{id}@path={Uri.EscapeDataString(key)}";
        }

        public override Task DeleteEntry(string id, string key, Dictionary<string, string> options = null)
        {
            var graph = _rdfStoreConnector.GetGraph(GetGraphName(id, key));

            if (!graph.IsEmpty)
            {
                _rdfStoreConnector.DeleteGraph(GetGraphName(id, key));
            }

            return Task.CompletedTask;
        }

        public override Task<ResourceEntry> GetEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var graph = _rdfStoreConnector.GetGraph(GetGraphName(id, key));

            if (graph.IsEmpty)
            {
                return Task.FromResult<ResourceEntry>(null);
            }

            var triples = graph.GetTriplesWithSubjectPredicate(graph.CreateUriNode(new Uri(GetSubjectName(id, key))), graph.CreateUriNode(new Uri(_predicate)));
            
            if(triples.Count() == 0)
            {
                return Task.FromResult<ResourceEntry>(null);
            }

            return Task.FromResult(new ResourceEntry(key, true, Encoding.UTF8.GetBytes(triples.First().Object.ToString()).Length, null, null, new DateTime(), new DateTime()));
        }

        public override Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string> options = null)
        {
            // Should be moved to rdfStoreConnector, when it gets updated.
            var cmdString = new SparqlParameterizedString
            {
                CommandText = $@"SELECT DISTINCT ?g 
                    WHERE {{
                      GRAPH ?g {{ ?s ?p ?o }}

                      FILTER regex(?g, ""^{GetGraphNameForFilter(id, prefix)}"")
                    }}"
            };
            var resultSet = _rdfStoreConnector.QueryEndpoint.QueryWithResultSet(cmdString.ToString());

            // Extract the actual key from the graphname and get the entry
            var resultList = resultSet.Select(x =>
            { 
                var k = x.Value("g").ToString();
                k = k.Substring($"{_urlPrefix}{_prefix}/{id}@path=".Length);
                if (k.Contains("&data"))
                {
                    k = k.Substring(0, k.Length - "&data".Length);
                }
                k = Uri.UnescapeDataString(k);
                return GetEntry(id, k).Result;
            }).ToList();

            return Task.FromResult(resultList);
        }

        public override Task<Stream> LoadEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var graph = _rdfStoreConnector.GetGraph(GetGraphName(id, key)); 
            
            if (graph.IsEmpty)
            {
                return Task.FromResult<Stream>(null);
            }

            var triples = graph.GetTriplesWithSubjectPredicate(graph.CreateUriNode(new Uri(GetSubjectName(id, key))), graph.CreateUriNode(new Uri(_predicate)));

            if (triples.Count() == 0)
            {
                return Task.FromResult<Stream>(null);
            }

            byte[] byteArray = Encoding.UTF8.GetBytes(triples.First().Object.ToString());
            var stream = new MemoryStream(byteArray);

            return Task.FromResult<Stream>(stream);
        }

        public override Task StoreEntry(string id, string key, Stream body, Dictionary<string, string> options = null)
        {
            // 4kb + 1
            byte[] buffer = new byte[1024 * 4 + 1];
            var size = body.Read(buffer, 0, buffer.Length);

            if(size > buffer.Length - 1)
            {
                throw new Exception("Cannot save stream. The stream has more than 4 kb of data.");
            }

            Array.Resize(ref buffer, size);

            var graph = _rdfStoreConnector.GetGraph(GetGraphName(id, key));

            if (!graph.IsEmpty)
            {
                var triples = graph.GetTriplesWithSubjectPredicate(graph.CreateUriNode(new Uri(GetSubjectName(id, key))), graph.CreateUriNode(new Uri(_predicate)));
                graph.Retract(triples.ToArray());
            }
            else
            {
                _rdfStoreConnector.CreateNamedGraph(GetGraphName(id, key));
            }

            graph.Assert(new Triple(graph.CreateUriNode(new Uri(GetSubjectName(id, key))), graph.CreateUriNode(new Uri(_predicate)), graph.CreateLiteralNode(Encoding.UTF8.GetString(buffer))));
            
            _rdfStoreConnector.AddGraph(graph);

            return Task.CompletedTask;
        }

        public override async Task<ResourceTypeInformation> GetResourceTypeInformation()
        {
            var resourceTypeInformation = await base.GetResourceTypeInformation();
            resourceTypeInformation.ResourceContent.MetadataView.EditableDataUrl = true;
            resourceTypeInformation.ResourceContent.MetadataView.EditableKey = true;
            resourceTypeInformation.IsQuotaAvailable = false;
            resourceTypeInformation.IsQuotaAdjustable = false;
            return await Task.FromResult(resourceTypeInformation);
        }
    }
}
