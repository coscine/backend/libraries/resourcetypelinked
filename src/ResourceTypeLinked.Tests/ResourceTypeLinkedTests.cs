﻿using Coscine.Configuration;
using Coscine.ResourceLoader;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeLinked.Tests
{
    [TestFixture]
    public class ResourceTypeLinkedTests
    {
        private ResourceTypeLinked _resourceTypeLinked = new ResourceTypeLinked("linked", new ConsulConfiguration(), null);
        private Guid id = Guid.NewGuid();
        private readonly string key = "/test.txt";

        private string StreamToString(Stream stream)
        {
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        private Stream StringToStream(string s)
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            return new MemoryStream(byteArray);
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            //id = new Guid("38067506-cfd7-499d-a066-929a23139bd1");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            foreach (var g in _resourceTypeLinked.ListEntries(id.ToString(), "/test").Result)
            {
                _resourceTypeLinked.DeleteEntry(id.ToString(), g.Key).Wait();
            }

            // Should the list feature no work
            if(_resourceTypeLinked.LoadEntry(id.ToString(), key).Result != null)
            {
                _resourceTypeLinked.DeleteEntry(id.ToString(), key).Wait();
            }

            if (_resourceTypeLinked.LoadEntry(id.ToString(), "/testA.txt").Result != null)
            {
                _resourceTypeLinked.DeleteEntry(id.ToString(), "/testA.txt").Wait();
            }

            if (_resourceTypeLinked.LoadEntry(id.ToString(), "/testB.txt").Result != null)
            {
                _resourceTypeLinked.DeleteEntry(id.ToString(), "/testB.txt").Wait();
            }
        }

        [Test]
        public void TestConstructor()
        {
            _ = new ResourceTypeLinked("linked", new ConsulConfiguration(), null);
        }

        [Test]
        public async Task ListEntries()
        {
            var textA = "TestText A";
            var textB = "TestText B ÄÖÜ";

            Assert.True((await _resourceTypeLinked.ListEntries(id.ToString(), "/test")).Count == 0);
            await _resourceTypeLinked.StoreEntry(id.ToString(), "/testA.txt", StringToStream(textA));
            await _resourceTypeLinked.StoreEntry(id.ToString(), "/testB.txt", StringToStream(textB));
            Assert.True((await _resourceTypeLinked.ListEntries(id.ToString(), "/test")).Count == 2);
            await _resourceTypeLinked.DeleteEntry(id.ToString(), "/testA.txt");
            await _resourceTypeLinked.DeleteEntry(id.ToString(), "/testB.txt");
            Assert.True((await _resourceTypeLinked.ListEntries(id.ToString(), "/test")).Count == 0);
        }

        [Test]
        public async Task CompleteTest()
        {
            var textA = "TestText A";
            var textB = "TestText B ÄÖÜ";
            // Max size/length
            var bufferSize = 1024 * 4;

            await _resourceTypeLinked.StoreEntry(id.ToString(), key, StringToStream(textA));
            Assert.True(StreamToString(await _resourceTypeLinked.LoadEntry(id.ToString(), key)) == textA);
            Assert.True((await _resourceTypeLinked.GetEntry(id.ToString(), key)).BodyBytes == Encoding.UTF8.GetBytes(textA).Length);


            await _resourceTypeLinked.StoreEntry(id.ToString(), key, StringToStream(textB));
            Assert.True(StreamToString(await _resourceTypeLinked.LoadEntry(id.ToString(), key)) == textB);
            Assert.True((await _resourceTypeLinked.GetEntry(id.ToString(), key)).BodyBytes == Encoding.UTF8.GetBytes(textB).Length);


            byte[] buffer = new byte[bufferSize];
            var random = new Random();
            random.NextBytes(buffer);
            // For saving binary data to a string, UTF8 and string escaping is needed. This will increase the size!
            buffer = Encoding.UTF8.GetBytes(Uri.EscapeDataString(Encoding.UTF8.GetString(buffer)));

            // Resize back to the maximum Size
            Array.Resize(ref buffer, bufferSize);

            byte[] buffer2 = new byte[buffer.Length];

            await _resourceTypeLinked.StoreEntry(id.ToString(), key, new MemoryStream(buffer));
            Assert.True((await _resourceTypeLinked.GetEntry(id.ToString(), key)).BodyBytes == buffer.Length);
            var read = (await _resourceTypeLinked.LoadEntry(id.ToString(), key)).Read(buffer2, 0, buffer2.Length);
            Assert.True(read == buffer.Length);
            Assert.True(buffer.SequenceEqual(buffer2));

            await _resourceTypeLinked.DeleteEntry(id.ToString(), key);
            Assert.True(await _resourceTypeLinked.LoadEntry(id.ToString(), key) == null);
            Assert.True(await _resourceTypeLinked.GetEntry(id.ToString(), key) == null);
        }

        [Test]
        public void MaxSize()
        {
            byte[] buffer = new byte[4 * 1024 + 1];
            Random r = new Random();
            r.NextBytes(buffer);

            Assert.Throws<Exception>(() => { _resourceTypeLinked.StoreEntry(id.ToString(), key, new MemoryStream(buffer)).Wait(); });
        }

        [Test]
        public void TestResourceTypeInformation()
        {
            // Load the current assembly from the ref and not the locally installed.
            var assemblyName = Assembly.GetExecutingAssembly().GetReferencedAssemblies().Where(x => x.Name == "Coscine.ResourceTypeLinked").FirstOrDefault();
            var assembly = Assembly.Load(assemblyName);
            var resourceType = ResourceTypeFactory.CreateResourceTypeObject("linked", new ConsulConfiguration(), assembly);
            var resourceTypeInformation = resourceType.GetResourceTypeInformation().Result;
            Assert.IsFalse(resourceTypeInformation.IsQuotaAvailable);
            Assert.IsFalse(resourceTypeInformation.IsQuotaAdjustable);
        }
    }
}
